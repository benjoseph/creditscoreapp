package me.benjoseph.credit_score_chart.viewmodel;

import android.content.Context;

import java.util.ArrayList;
import java.util.List;

import me.benjoseph.credit_score_chart.model.CreditScoreBandData;
import me.benjoseph.credit_score_chart.model.CreditScoreData;

public class Repository {

    private CreditScoreData mCreditScoreData;
    private CreditScoreBandData[] mCreditScoreBandData;

    interface Callback {
        void onSuccess(Object object);

        void onError(Object error);
    }

    public Repository(Context context) {
        int color1 = context.getResources().getColor(me.benjoseph.credit_score_chart.R.color.one);
        int color2 = context.getResources().getColor(me.benjoseph.credit_score_chart.R.color.two);
        int color3 = context.getResources().getColor(me.benjoseph.credit_score_chart.R.color.three);
        int color4 = context.getResources().getColor(me.benjoseph.credit_score_chart.R.color.four);
        int color5 = context.getResources().getColor(me.benjoseph.credit_score_chart.R.color.five);

        CreditScoreBandData band1 = new CreditScoreBandData(300, 699, 19, false, 0, color5);
        CreditScoreBandData band2 = new CreditScoreBandData(700, 774, 21, false, 0, color4);
        CreditScoreBandData band3 = new CreditScoreBandData(775, 799, 21, false, 0, color3);
        CreditScoreBandData band4 = new CreditScoreBandData(800, 824, 20, true, 820, color2);
        CreditScoreBandData band5 = new CreditScoreBandData(825, 900, 19, false, 0, color1);

        ArrayList<CreditScoreBandData> creditScoreBandData = new ArrayList<>();
        creditScoreBandData.add(band5);
        creditScoreBandData.add(band4);
        creditScoreBandData.add(band3);
        creditScoreBandData.add(band2);
        creditScoreBandData.add(band1);

        mCreditScoreData = new CreditScoreData(800, 900, 300);
        mCreditScoreBandData = creditScoreBandData.toArray(new CreditScoreBandData[0]);
    }

    public void getCreditScoreData(Callback callback) {
        callback.onSuccess(mCreditScoreData);
    }

    public void getCreditScoreBandData(Callback callback) {
        callback.onSuccess(mCreditScoreBandData);
    }
}
