package me.benjoseph.credit_score_chart.viewmodel;

import android.content.Context;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import me.benjoseph.credit_score_chart.model.CreditScoreBandData;
import me.benjoseph.credit_score_chart.model.CreditScoreData;

public class CreditScoreViewModel extends ViewModel {

    private MutableLiveData<CreditScoreData> mCreditScoreDataMutableLiveData;
    private MutableLiveData<CreditScoreBandData[]> mCreditScoreBandMutableLiveData;
    private Repository mRepository;

    public CreditScoreViewModel(Context context) {
        mRepository = new Repository(context);
        mCreditScoreDataMutableLiveData = new MutableLiveData<>();
        mCreditScoreBandMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<CreditScoreData> getCreditScoreDataMutableLiveData() {
        return mCreditScoreDataMutableLiveData;
    }

    public MutableLiveData<CreditScoreBandData[]> getCreditScoreBandListMutableLiveData() {
        return mCreditScoreBandMutableLiveData;
    }

    public void getCreditScoreData() {
        Repository.Callback callback = new Repository.Callback() {
            @Override
            public void onSuccess(Object object) {
                CreditScoreData creditScoreData = (CreditScoreData) object;
                mCreditScoreDataMutableLiveData.postValue(creditScoreData);
            }

            @Override
            public void onError(Object error) {

            }
        };
        mRepository.getCreditScoreData(callback);
    }

    public void getCreditScoreBandData() {
        Repository.Callback callback = new Repository.Callback() {
            @Override
            public void onSuccess(Object object) {
                CreditScoreBandData[] creditScoreBandData = (CreditScoreBandData[]) object;
                mCreditScoreBandMutableLiveData.postValue(creditScoreBandData);
            }

            @Override
            public void onError(Object error) {

            }
        };
        mRepository.getCreditScoreBandData(callback);
    }


}
