package me.benjoseph.credit_score_chart.view;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

import me.benjoseph.credit_score_chart.model.CreditScoreBandData;

public class CreditScoreBandRecyclerViewAdapter extends RecyclerView.Adapter<CreditScoreBandRecyclerViewAdapter.ViewHolder> {

    private List<CreditScoreBandData> mDataSet;

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CreditScoreBandItemView mCreditScoreBandItemView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            mCreditScoreBandItemView = (CreditScoreBandItemView)itemView;
        }

        public void setupView(CreditScoreBandData creditScoreBandData) {
            mCreditScoreBandItemView.setupView(creditScoreBandData);
        }
    }

    public CreditScoreBandRecyclerViewAdapter(List<CreditScoreBandData> creditScoreBandData) {
        mDataSet = creditScoreBandData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        CreditScoreBandItemView creditScoreBandItemView = new CreditScoreBandItemView(parent.getContext());
        creditScoreBandItemView.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        return new ViewHolder(creditScoreBandItemView);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.setupView(mDataSet.get(position));
    }

    @Override
    public int getItemCount() {
        return mDataSet.size();
    }
}
