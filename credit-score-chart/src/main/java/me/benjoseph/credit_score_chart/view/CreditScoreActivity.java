package me.benjoseph.credit_score_chart.view;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import java.text.SimpleDateFormat;
import java.util.Date;

import me.benjoseph.credit_score_chart.R;
import me.benjoseph.credit_score_chart.model.CreditScoreData;
import me.benjoseph.credit_score_chart.viewmodel.CreditScoreViewModel;
import me.benjoseph.credit_score_chart.viewmodel.CreditScoreViewModelFactory;

public class CreditScoreActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_score);

        CreditScoreViewModelFactory viewModelFactory = new CreditScoreViewModelFactory(getBaseContext());
        CreditScoreViewModel creditScoreViewModel =
            new ViewModelProvider(this, viewModelFactory).get(CreditScoreViewModel.class);

        LiveData<CreditScoreData> creditScoreDataLiveData = creditScoreViewModel.getCreditScoreDataMutableLiveData();
        creditScoreDataLiveData.observe(this, new Observer<CreditScoreData>() {
            @Override
            public void onChanged(CreditScoreData creditScoreData) {
                setupView(creditScoreData);
            }
        });
        creditScoreViewModel.getCreditScoreData();
    }

    private void setupView(CreditScoreData creditScoreData) {
        LinearLayout chartHolder = findViewById(R.id.credit_score_chart_holder);
        chartHolder.addView(new CreditScoreView(this, creditScoreData));

        TextView dateText = findViewById(R.id.credit_score_date_line);
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        Date currentDate = new Date();
        String dateString = simpleDateFormat.format(currentDate);
        dateText.setText("As of " + dateString);

        Button analysisButton = findViewById(R.id.credit_score_analysis_button);

        final Intent creditScoreBandIntent = new Intent(CreditScoreActivity.this, CreditScoreBandActivity.class);

        analysisButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(creditScoreBandIntent);
            }
        });
    }
}