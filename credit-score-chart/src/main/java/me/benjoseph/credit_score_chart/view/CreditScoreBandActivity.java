package me.benjoseph.credit_score_chart.view;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.Arrays;
import java.util.List;

import me.benjoseph.credit_score_chart.R;
import me.benjoseph.credit_score_chart.model.CreditScoreBandData;
import me.benjoseph.credit_score_chart.viewmodel.CreditScoreViewModel;
import me.benjoseph.credit_score_chart.viewmodel.CreditScoreViewModelFactory;

public class CreditScoreBandActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_credit_score_band);

        CreditScoreViewModelFactory viewModelFactory = new CreditScoreViewModelFactory(getBaseContext());
        CreditScoreViewModel creditScoreViewModel =
            new ViewModelProvider(this, viewModelFactory).get(CreditScoreViewModel.class);

        LiveData<CreditScoreBandData[]> creditScoreBandListLiveData =
            creditScoreViewModel.getCreditScoreBandListMutableLiveData();
        creditScoreBandListLiveData.observe(this, new Observer<CreditScoreBandData[]>() {
            @Override
            public void onChanged(CreditScoreBandData[] creditScoreData) {
                List<CreditScoreBandData> creditScoreBandData = Arrays.asList(creditScoreData);

                setupView(creditScoreBandData);
            }
        });
        creditScoreViewModel.getCreditScoreBandData();
    }

    private void setupView(List<CreditScoreBandData> creditScoreBandData) {
        CreditScoreBandRecyclerViewAdapter adapter = new CreditScoreBandRecyclerViewAdapter(creditScoreBandData);
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        RecyclerView recyclerView = findViewById(R.id.recycler_view);
        VerticalSpaceItemDecoration verticalSpaceItemDecoration = new VerticalSpaceItemDecoration(30);
        recyclerView.addItemDecoration(verticalSpaceItemDecoration);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }
}