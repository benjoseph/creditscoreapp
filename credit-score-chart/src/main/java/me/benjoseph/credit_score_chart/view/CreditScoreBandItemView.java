package me.benjoseph.credit_score_chart.view;

import android.content.Context;
import android.view.View;
import android.widget.TextView;

import androidx.constraintlayout.widget.ConstraintLayout;

import me.benjoseph.credit_score_chart.model.CreditScoreBandData;
import me.benjoseph.credit_score_chart.R;

public class CreditScoreBandItemView extends ConstraintLayout {

    private TextView percentageTv;
    private TextView bandTv;
    private TextView scoreTv;
    private ConstraintLayout constraintLayout;

    public CreditScoreBandItemView(Context context) {
        super(context);
        View.inflate(context, R.layout.credit_score_band_item, this);
        constraintLayout = (ConstraintLayout)getChildAt(0);
        percentageTv = (TextView)constraintLayout.getChildAt(0);
        bandTv = (TextView)constraintLayout.getChildAt(1);
        scoreTv = (TextView)constraintLayout.getChildAt(2);
    }

    public void setPercentageText(String percentage) {
        percentageTv.setText(percentage);
    }

    public void setBandText(String band) {
        bandTv.setText(band);
    }

    public void setBandColor(int color) {
        bandTv.setBackgroundColor(color);
    }

    public void setupView(CreditScoreBandData creditScoreBandData) {
        constraintLayout.setBackgroundColor(creditScoreBandData.getBandColor());
        percentageTv.setText(creditScoreBandData.getPercentage()+"%");
        bandTv.setText(creditScoreBandData.getLowerLimit()+"-"+creditScoreBandData.getHigherLimit());
        if(creditScoreBandData.isScoreBand()) {
            scoreTv.setVisibility(VISIBLE);
            scoreTv.setText(String.valueOf(creditScoreBandData.getScore()));
        }
    }
}
