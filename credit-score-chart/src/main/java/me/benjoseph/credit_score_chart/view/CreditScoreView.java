package me.benjoseph.credit_score_chart.view;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.util.Log;
import android.view.View;

import me.benjoseph.credit_score_chart.model.CreditScoreData;
import me.benjoseph.credit_score_chart.R;
import me.benjoseph.credit_score_chart.Util;

public class CreditScoreView extends View {

    int lowerLimit;
    int higherLimit;
    float score;
    int scoreAngle;

    public CreditScoreView(Context context, CreditScoreData creditScoreData) {
        super(context);

        lowerLimit = creditScoreData.getLowerLimit();
        higherLimit = creditScoreData.getHigherLimit();
        score = creditScoreData.getCreditScore();

        float scorePercent = ((score-lowerLimit)/(higherLimit-lowerLimit));
        Log.e("ben", "score percemt is:"+ scorePercent);
        scoreAngle = (int) (scorePercent*270);
        Log.e("ben", "score angle is:"+ scoreAngle);
    }



    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        float width = (float) getWidth();
        float height = (float) getHeight();
        float radius;

        if (width > height) {
            radius = height / 4;
        } else {
            radius = width / 4;
        }

        int strokeWidth = dp(1);
        int orangeColor = getResources().getColor(R.color.orange);
        Paint scorePaint = new Paint();
        scorePaint.setColor(orangeColor);
        scorePaint.setStrokeWidth(strokeWidth);
        scorePaint.setStyle(Paint.Style.FILL);
        scorePaint.setStyle(Paint.Style.STROKE);

        Paint rangePaint = new Paint();
        rangePaint.setColor(Color.GRAY);
        rangePaint.setStrokeWidth(strokeWidth);
        rangePaint.setStyle(Paint.Style.FILL);
        rangePaint.setStyle(Paint.Style.STROKE);

        Paint rangeTextPaint = new Paint();
        rangeTextPaint.setTextSize(dp(1));
        rangeTextPaint.setColor(Color.BLACK);
        rangeTextPaint.setStyle(Paint.Style.FILL);

        Paint scoreTextPaint = new Paint();
        scoreTextPaint.setTextSize(dp(4));
        scoreTextPaint.setColor(orangeColor);
        scoreTextPaint.setStyle(Paint.Style.FILL);

        float center_x, center_y;
        final RectF oval = new RectF();

        center_x = width / 2;
        center_y = height / 2;

        oval.set(center_x - radius,
            center_y - radius,
            center_x + radius,
            center_y + radius);
        canvas.drawArc(oval, 90, 270, false, rangePaint);
        canvas.drawArc(oval, 90, scoreAngle, false, scorePaint);

        int textPadding = 20;
        canvas.drawText(String.valueOf(lowerLimit), (width/2)+textPadding, (height/2)+radius +(strokeWidth/2),
            rangeTextPaint);
        canvas.drawText(String.valueOf(higherLimit), (width/2) + radius - strokeWidth - strokeWidth/2,
            (height/2)+ textPadding +strokeWidth,
            rangeTextPaint);
        canvas.drawText(String.valueOf((int)score),center_x-((dp(4))), center_y+(dp(4)/2), scoreTextPaint);
    }

    private int dp(int dp) {
        return Util.getPixelsForDp(getContext(), dp);
    }
}

