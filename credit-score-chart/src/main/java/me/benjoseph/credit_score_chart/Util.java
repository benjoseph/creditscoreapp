package me.benjoseph.credit_score_chart;

import android.content.Context;

public class Util {

    // The gesture threshold expressed in dp
    private static final float GESTURE_THRESHOLD_DP = 16.0f;

    public static int getPixelsForDp(Context context, int dp) {
        // Get the screen's density scale
        final float scale = context.getResources().getDisplayMetrics().density;
        // Convert the dps to pixels, based on density scale
        return (int) (GESTURE_THRESHOLD_DP * scale + 0.5f) * dp;
    }

}
