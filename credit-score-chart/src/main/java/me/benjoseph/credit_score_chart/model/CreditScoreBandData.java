package me.benjoseph.credit_score_chart.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CreditScoreBandData  implements Parcelable {
    private int lowerLimit;
    private int higherLimit;
    private int percentage;
    private boolean scoreBand;
    private int score;
    private int bandColor;

    public CreditScoreBandData(int lowerLimit, int higherLimit, int percentage, boolean scoreBand, int score, int bandColor) {
        this.lowerLimit = lowerLimit;
        this.higherLimit = higherLimit;
        this.percentage = percentage;
        this.scoreBand = scoreBand;
        this.score = score;
        this.bandColor = bandColor;
    }

    protected CreditScoreBandData(Parcel in) {
        lowerLimit = in.readInt();
        higherLimit = in.readInt();
        percentage = in.readInt();
        scoreBand = in.readByte() != 0;
        score = in.readInt();
        bandColor = in.readInt();
    }

    public static final Creator<CreditScoreBandData> CREATOR = new Creator<CreditScoreBandData>() {
        @Override
        public CreditScoreBandData createFromParcel(Parcel in) {
            return new CreditScoreBandData(in);
        }

        @Override
        public CreditScoreBandData[] newArray(int size) {
            return new CreditScoreBandData[size];
        }
    };

    public int getLowerLimit() {
        return lowerLimit;
    }

    public int getHigherLimit() {
        return higherLimit;
    }

    public int getPercentage() {
        return percentage;
    }

    public boolean isScoreBand() {
        return scoreBand;
    }

    public int getScore() {
        return score;
    }

    public int getBandColor() {
        return bandColor;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(lowerLimit);
        dest.writeInt(higherLimit);
        dest.writeInt(percentage);
        dest.writeByte((byte) (scoreBand ? 1 : 0));
        dest.writeInt(score);
        dest.writeInt(bandColor);
    }
}
