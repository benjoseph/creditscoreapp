package me.benjoseph.credit_score_chart.model;

import android.os.Parcel;
import android.os.Parcelable;

public class CreditScoreData implements Parcelable {

    private int creditScore;
    private int higherLimit;
    private int lowerLimit;

    public CreditScoreData(int creditScore, int higherLimit, int lowerLimit) {
        this.creditScore = creditScore;
        this.higherLimit = higherLimit;
        this.lowerLimit = lowerLimit;
    }

    protected CreditScoreData(Parcel in) {
        creditScore = in.readInt();
        higherLimit = in.readInt();
        lowerLimit = in.readInt();
    }

    public static final Creator<CreditScoreData> CREATOR = new Creator<CreditScoreData>() {
        @Override
        public CreditScoreData createFromParcel(Parcel in) {
            return new CreditScoreData(in);
        }

        @Override
        public CreditScoreData[] newArray(int size) {
            return new CreditScoreData[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(creditScore);
        dest.writeInt(higherLimit);
        dest.writeInt(lowerLimit);
    }

    public int getCreditScore() {
        return creditScore;
    }

    public int getHigherLimit() {
        return higherLimit;
    }

    public int getLowerLimit() {
        return lowerLimit;
    }
}
