package me.benjoseph.creditscore;

import android.content.Intent;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import me.benjoseph.credit_score_chart.view.CreditScoreActivity;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Intent creditScoreIntent = new Intent(this, CreditScoreActivity.class);
        startActivity(creditScoreIntent);
    }
}